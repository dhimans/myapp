.controller('UserMessagesCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'MockService', 'CommandsService', '$ionicActionSheet', '$ionicPopup', '$ionicScrollDelegate', '$timeout', '$interval', '$ionicModal', function($scope, $rootScope, $state, $stateParams, MockService, CommandsService, $ionicActionSheet,
    $ionicPopup, $ionicScrollDelegate, $timeout, $interval, $ionicModal) {
    
    $scope.groupId = localStorage.getItem('groupId');
    $scope.id = localStorage.getItem('id');
    $scope.type = parseInt(localStorage.getItem('type'));
    
    if(localStorage.getItem('polls') != '' && localStorage.getItem('polls') != null && localStorage.getItem('polls') != undefined) {
        $scope.polls = JSON.parse(localStorage.getItem('polls'));
    } else {
        $scope.polls = pollsJSON;
        localStorage.setItem('polls', JSON.stringify($scope.polls));
    }
    
    if(localStorage.getItem('reminders') != '' && localStorage.getItem('reminders') != null && localStorage.getItem('reminders') != undefined) {
        $scope.reminders = JSON.parse(localStorage.getItem('reminders'));
    } else {
        $scope.reminders = remindersJSON;
        localStorage.setItem('reminders', JSON.stringify($scope.reminders));
    }
    
    if(localStorage.getItem('todos') != '' && localStorage.getItem('todos') != null && localStorage.getItem('todos') != undefined) {
        $scope.todos = JSON.parse(localStorage.getItem('todos'));
    } else {
        $scope.todos = todosJSON;
        localStorage.setItem('todos', JSON.stringify($scope.todos));
    }
    
    if($scope.type == 1) {
        if(localStorage.getItem('groups') != '' && localStorage.getItem('groups') != null && localStorage.getItem('groups') != undefined) {
            $scope.groups = JSON.parse(localStorage.getItem('groups'));
            $scope.group = search($scope.groupId, $scope.groups);
        }
        // mock acquiring data via $stateParams
        $scope.toUser = {
            _id: '534b8e5aaa5e7afc1b23e69b',
            pic: 'http://ionicframework.com/img/docs/venkman.jpg',
            username: $scope.group.teamName
        }
    } else {
        if(localStorage.getItem('employees') != '' && localStorage.getItem('employees') != null && localStorage.getItem('employees') != undefined) {
            $scope.employees = JSON.parse(localStorage.getItem('employees'));
            $scope.employee = search($scope.id, $scope.employees);
            console.log($scope.employee)
        }
        
        $scope.toUser = {
            _id: '534b8e5aaa5e7afc1b23e69b',
            pic: 'pics/' + $scope.employee.pic,
            username: $scope.employee.firstName + " " + $scope.employee.lastName
        }
    }

    // this could be on $rootScope rather than in $stateParams
    $scope.user = {
        _id: '534b8fb2aa5e7afc1b23e69c',
        pic: 'pics/dhiman.jpg',
        username: 'Dhiman'
    };

    $scope.input = {
        message: localStorage['userMessage-' + $scope.toUser._id] || ''
    };

    var messageCheckTimer;

    var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');
    var footerBar; // gets set in $ionicView.enter
    var scroller;
    var txtInput; // ^^^

    $scope.$on('$ionicView.enter', function() {
        console.log('UserMessages $ionicView.enter');

        getMessages();
      
        $timeout(function() {
            footerBar = document.body.querySelector('#userMessagesView .bar-footer');
            scroller = document.body.querySelector('#userMessagesView .scroll-content');
            txtInput = angular.element(footerBar.querySelector('textarea'));
        }, 0);

        messageCheckTimer = $interval(function() {
            // here you could check for new messages if your app doesn't use push notifications or user disabled them
        }, 20000);
    });

    $scope.$on('$ionicView.leave', function() {
        console.log('leaving UserMessages view, destroying interval');
        // Make sure that the interval is destroyed
        if (angular.isDefined(messageCheckTimer)) {
            $interval.cancel(messageCheckTimer);
            messageCheckTimer = undefined;
        }
    });

    $scope.$on('$ionicView.beforeLeave', function() {
        if (!$scope.input.message || $scope.input.message === '') {
            localStorage.removeItem('userMessage-' + $scope.toUser._id);
        }
    });

    function getMessages() {
        // the service is mock but you would probably pass the toUser's GUID here
        MockService.getUserMessages({
            toUserId: $scope.toUser._id
        }).then(function(data) {
            $scope.doneLoading = true;
            $scope.messages = data.messages;

            $timeout(function() {
                viewScroll.scrollBottom();
            }, 0);
        });
    }

    $scope.$watch('input.message', function(newValue, oldValue) {
        console.log('input.message $watch, newValue ' + newValue);
        if (!newValue) newValue = '';
        localStorage['userMessage-' + $scope.toUser._id] = newValue;
    });
    
    $scope.sendMessage = function(sendMessageForm) {
        var message = {
        toId: $scope.toUser._id,
        text: $scope.input.message
      };

      // if you do a web service call this will be needed as well as before the viewScroll calls
      // you can't see the effect of this in the browser it needs to be used on a real device
      // for some reason the one time blur event is not firing in the browser but does on devices
      keepKeyboardOpen();
      
      //MockService.sendMessage(message).then(function(data) {
      $scope.input.message = '';

      message._id = new Date().getTime(); // :~)
      message.date = new Date();
      message.username = $scope.user.username;
      message.userId = $scope.user._id;
      message.pic = $scope.user.picture;

      $scope.messages.push(message);

      $timeout(function() {
        keepKeyboardOpen();
        viewScroll.scrollBottom(true);
      }, 0);

      $timeout(function() {
        $scope.messages.push(MockService.getMockMessage());
        keepKeyboardOpen();
        viewScroll.scrollBottom(true);
      }, 2000);

      //});
    };

    $scope.sendMessage = function(sendMessageForm) {
        var message = {};

        // if you do a web service call this will be needed as well as before the viewScroll calls
        // you can't see the effect of this in the browser it needs to be used on a real device
        // for some reason the one time blur event is not firing in the browser but does on devices
        keepKeyboardOpen();
        var text = $scope.input.message;
        
        if($scope.isCommandSelected == true) {
            
            if($scope.selectedCommand == 'reminder') {
                
                text = text.split(' ');
                var reminderText = text.splice(5, (text.length - 1)).join(" ");
                
                $scope.reminder = {
                    'id': 'reminder' + $scope.reminders.length + 1,
                    'message': reminderText,
                    'time': text[3] + " " + text[4],
                    'recipient': text[1],
                    'isCompleted': false
                };
                
                $scope.reminders.push($scope.reminder);
                localStorage.setItem('reminders', JSON.stringify($scope.reminders));
                message.type = 4;
                message = {
                    toId: $scope.toUser._id,
                    text: reminderText
                };
                
            } else if($scope.selectedCommand == 'poll') {
                
                text = text.split('q:');
                var pollText = text[1];
                pollText = pollText.split('o:');
                var quesText = pollText[0];
                var options = pollText[1];
                options = options.split(', ');
                    
                $scope.poll = {
                    'id': 'poll' + $scope.polls.length + 1,
                    'question': quesText,
                    'completed': false,
                    'votes': 0,
                    'isAnswered': false,
                    'options': [
                        {
                            'id': 'optn1',
                            'optionText': options[0],
                            'image': ''
                        }, {
                            'id': 'optn2',
                            'optionText': options[1],
                            'image': ''
                        }, {
                            'id': 'optn3',
                            'optionText': options[2],
                            'image': ''
                        }, {
                            'id': 'optn4',
                            'optionText': options[3],
                            'image': ''
                        }
                    ]
                };
                
                $scope.polls.push($scope.poll);
                localStorage.setItem('polls', JSON.stringify($scope.polls));
                message.type = 2;
                message = {
                    toId: $scope.toUser._id,
                    text: quesText
                };
                
            } else {
                
                text = text.split('t:');
                var todoText = text[1];
                $scope.todo = {
                    'id': 'todo' + $scope.todos.length + 1,
                    'title': todoText,
                    'completed': false
                };
                
                $scope.todos.push($scope.todo);
                localStorage.setItem('todos', JSON.stringify($scope.todos));
                message.type = 5;
                message = {
                    toId: $scope.toUser._id,
                    text: todoText
                };
            }
            
            
        } else {
            message.type = 1;
            message = {
                toId: $scope.toUser._id,
                text: $scope.input.message
            };
            console.log("sdsfsdfsdfs")
            console.log(message)
        }
      
        //MockService.sendMessage(message).then(function(data) {
        $scope.input.message = '';

        message._id = new Date().getTime(); // :~)
        message.date = new Date();
        message.username = $scope.user.username;
        message.userId = $scope.user._id;
        message.pic = $scope.user.picture;
        
        console.log('message');
        console.log(JSON.stringify(message));
        
        $scope.messages.push(message);
        

        $timeout(function() {
            keepKeyboardOpen();
            viewScroll.scrollBottom(true);
        }, 0);

        $timeout(function() {
            $scope.messages.push(MockService.getMockMessage());
            keepKeyboardOpen();
            viewScroll.scrollBottom(true);
        }, 2000);

      //});
    };
    
    // this keeps the keyboard open on a device only after sending a message, it is non obtrusive
    function keepKeyboardOpen() {
        console.log('keepKeyboardOpen');
        txtInput.one('blur', function() {
            console.log('textarea blur, focus back on it');
            txtInput[0].focus();
        });
    }

    $scope.onMessageHold = function(e, itemIndex, message) {
        console.log('onMessageHold');
        console.log('message: ' + JSON.stringify(message, null, 2));
        $ionicActionSheet.show({
            buttons: [{
                text: 'Copy Text'
            }, {
                text: 'Delete Message'
            }],
            buttonClicked: function(index) {
                switch (index) {
                    case 0: // Copy Text
                        //cordova.plugins.clipboard.copy(message.text);

                        break;
                    case 1: // Delete
                        // no server side secrets here :~)
                        $scope.messages.splice(itemIndex, 1);
                        $timeout(function() {
                            viewScroll.resize();
                        }, 0);

                        break;
                }
          
                return true;
            }
        });
    };

    // this prob seems weird here but I have reasons for this in my app, secret!
    $scope.viewProfile = function(msg) {
        if (msg.userId === $scope.user._id) {
            // go to your profile
        } else {
            // go to other users profile
        }
    };
    
    // I emit this event from the monospaced.elastic directive, read line 480
    $scope.$on('taResize', function(e, ta) {
        console.log('taResize');
        if (!ta) return;
      
        var taHeight = ta[0].offsetHeight;
        console.log('taHeight: ' + taHeight);
      
        if (!footerBar) return;
      
        var newFooterHeight = taHeight + 10;
        newFooterHeight = (newFooterHeight > 44) ? newFooterHeight : 44;
      
        footerBar.style.height = newFooterHeight + 'px';
        scroller.style.bottom = newFooterHeight + 'px'; 
    });
    
      
    $scope.selectedChannel = search($scope.id, $scope.group.channels);
    console.log($scope.selectedChannel)
      
    $scope.toggleGroup = function(group) {
        group.show = !group.show;
          
    };
    $scope.isGroupShown = function(group) {
        return group.show;
    };
      
    $scope.selectList = function(channel, group) {
        $scope.selectedChannel = channel.title;
        $scope.toggleGroup(group);
    };
      
      
    $ionicModal.fromTemplateUrl('templates/modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) { 
        $scope.modal = modal;
    });
      
      
    $scope.openModal = function() {
          $scope.modal.show();
    }
      
      
    $scope.OpenDetail = function() {
          $(".main-menu").removeClass("block").addClass("none");
          $(".sub-menu").removeClass("none").addClass("block");
    }
      
      
    $scope.shareFile = function(fileName) {
        var message = {
            toId: $scope.toUser._id,
            text: fileName
        };
      
        //MockService.sendMessage(message).then(function(data) {
        $scope.input.message = '';

        message._id = new Date().getTime(); // :~)
        message.date = new Date();
        message.username = $scope.user.username;
        message.userId = $scope.user._id;
        message.pic = $scope.user.picture;

        $scope.messages.push(message);

        $timeout(function() {
            $scope.modal.hide();
            keepKeyboardOpen();
            viewScroll.scrollBottom(true);
        }, 0);

        $timeout(function() {
            $scope.messages.push(MockService.getMockMessage());
            keepKeyboardOpen();
            viewScroll.scrollBottom(true);
        }, 2000);

      //});
    };
    
    /*$scope.data = { "airlines" : [], "search" : '' };
    
    $scope.search = function() {
        $(".content-mask, .command-pane").removeClass("none").addClass("block");
    	CommandsService.searchCommands($scope.data.search).then(
    		function(matches) {
                console.log(JSON.stringify(matches));
    			$scope.data.commands = matches;
    		}
    	)
    }
    
    
    $scope.selectCommand = function(command) {
        $(".content-mask, .command-pane").removeClass("block").addClass("none");
        $scope.isCommandSelected = true;
        $("#chat-box").val(command.title);
        $scope.selectedCommand = command.id;
    }*/
   

}])