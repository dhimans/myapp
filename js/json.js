var employeeJSON = [
            {
                'id': 1,
                'firstName': 'Akshay',
                'lastName': 'Valsa',
                'designation': 'Project manager',
                'pic':'John_Williams.jpg'
            },
            {
                'id': 2,
                'firstName': 'Ali',
                'lastName': 'Asger Saifuddin',
                'designation': 'Tech Lead',
                'pic':'Ray_Moore.jpg'
            },
            {
                'id': 3,
                'firstName': 'Ashish',
                'lastName': 'Shah',
                'designation': 'CEO',
                'pic':'James_King.jpg'
            },
            {
                'id': 4,
                'firstName': 'Ashwin',
                'lastName': 'Megha',
                'designation': 'COO',
                'pic':'Eugene_Lee.jpg'
            },
            {
                'id': 5,
                'firstName': 'Deepak',
                'lastName': 'Purohit',
                'designation': 'Project Manager',
                'pic':'Gary_Donovan.jpg'
            },
            {
                'id': 6,
                'firstName': 'Devesh',
                'lastName': 'Hingorani',
                'designation': 'CFO',
                'pic':'Paul_Jones.jpg'
            },
            {
                'id': 7,
                'firstName': 'Girish',
                'lastName': 'Vispute',
                'designation': 'Senior Software Engineer',
                'pic':'Kathleen_Byrne.jpg'
            },
            {
                'id': 8,
                'firstName': 'Susmita',
                'lastName': 'Kaushik',
                'designation': 'HR Manager',
                'pic':'Lisa_Wong.jpg'
            },
            {
                'id': 9,
                'firstName': 'Zohair',
                'lastName': 'Hassan',
                'designation': 'Delivery Manager',
                'pic':'Ray_Moore.jpg'
            }
        ];


var groupJSON = [
        {
            'id': 1,
            'teamName': 'Datviet',
            'membersCnt': 7,
            'channels': [
                {
                    'id': 'datCha1',
                    'title': '#general',
                    'description': ''
                }
            ],
            'pic':''
        }, {
            'id': 2,
            'teamName': 'Datviet',
            'membersCnt': 7,
            'channels': [
                {
                    'id': 'datCha1',
                    'title': '#general',
                    'description': ''
                }
            ],
            'pic':''
        },
        {
            'id': 3,
            'teamName': 'Noodletools',
            'membersCnt': 4,
            'channels': [
                {
                    'id': 'ntCha1',
                    'title': '#general',
                    'description': ''
                },
                {
                    'id': 'ntCha2',
                    'title': '#backbone',
                    'description': ''
                }
            ],
            'pic':''
        },
        {
            'id': 4,
            'teamName': 'Young Clay',
            'membersCnt': 6,
            'channels': [
                {
                    'id': 'ycCha1',
                    'title': '#general',
                    'description': ''
                },
                {
                    'id': 'ycCha2',
                    'title': '#android',
                    'description': ''
                },
                {
                    'id': 'ycCha3',
                    'title': '#server',
                    'description': ''
                }
            ],
            'pic':''
        }
    ];


var pollsJSON = [
            {
                'id': 'poll1',
                'question': 'Choice for Chair',
                'completed': true,
                'votes': 4,
                'isAnswered': false,
                'answer': 'optn1',
                'options': [
                    {
                        'id': 'optn1',
                        'optionText': 'Chair1',
                        'image': ''
                    }, {
                        'id': 'optn2',
                        'optionText': 'Chair2',
                        'image': ''
                    }, {
                        'id': 'optn3',
                        'optionText': 'Chair3',
                        'image': ''
                    }, {
                        'id': 'optn4',
                        'optionText': 'Chair4',
                        'image': ''
                    }
                ]
            },
            {
                'id': 'poll2',
                'question': 'Choice for Biriyani',
                'completed': false,
                'votes': 5,
                'isAnswered': true,
                'answer': 'optn4',
                'options': [
                    {
                        'id': 'optn1',
                        'optionText': 'Mutton Biriyani',
                        'image': ''
                    }, {
                        'id': 'optn2',
                        'optionText': 'Chicken Biriyani',
                        'image': ''
                    }, {
                        'id': 'optn3',
                        'optionText': 'Veg Biriyani',
                        'image': ''
                    }, {
                        'id': 'optn4',
                        'optionText': 'Paneer Biriyani',
                        'image': ''
                    }
                ]
            }
        ];


var todosJSON = [
            {
                'id': 'todo1',
                'title': 'Scuba Diving',
                'completed': true
            }, {
                'id': 'todo2',
                'title': 'Complete NodeJS assignment',
                'completed': false
            }
        ];


var notesJSON = [
    {
        'id': 'note1',
        'title': 'Sample Note',
        'htmlcontent': '<p>Lorem Ipsum Text</p>'
    }, {
        'id': 'note2',
        'title': 'Sample Note 1',
        'htmlcontent': '<p>Another Lorem Ipsum Text</p>'
    }
];


var remindersJSON = [
    {
        'id': 'reminder1',
        'message': 'Workflow design Meeting',
        'time': '30 mins',
        'recipient': 'all',
        'isCompleted': false
    }, {
        'id': 'reminder2',
        'message': 'Wake up reminder',
        'time': '60 mins',
        'recipient': 'me',
        'isCompleted': false
    }, {
        'id': 'reminder3',
        'message': 'Client call',
        'time': '2 hours',
        'recipient': 'all',
        'isCompleted': false
    }
];


var commands = [
    {
        "id":"poll",
        "title":"/poll",
        "subTitle":"q:question o:option 1, option 2, ...",
        "category": "Create a Poll"
    }, {
        "id":"reminder",
        "title":"/reminder",
        "subTitle":"[me or all] in [time e.g 30 mins] [message]",
        "category": "Create a Reminder"
    }, {
        "id":"todo",
        "title":"/todo",
        "subTitle":"t:to-do text",
        "category": "Create a todo"
    }
];


var messageJSON = {
    "messages":[
        {
            "_id":"535d625f898df4e80e2a125e",
            "text":"Ionic has changed the game for hybrid app development.",
            "userId":"534b8fb2aa5e7afc1b23e69c",
            "date":"2014-04-27T20:02:39.082Z",
            "read":true,
            "type": 1,
            "readDate":"2014-12-01T06:27:37.944Z"
        }, {
            "_id":"535f13ffee3b2a68112b9fc0",
            "text":"I like Ionic better than ice cream!",
            "userId":"534b8e5aaa5e7afc1b23e69b",
            "date":"2014-04-29T02:52:47.706Z",
            "read":true,
            "type": 1,
            "readDate":"2014-12-01T06:27:37.944Z"
        }, {
            "_id":"546a5843fd4c5d581efa263a",
            "text":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "userId":"534b8fb2aa5e7afc1b23e69c",
            "date":"2014-11-17T20:19:15.289Z",
            "read":true,
            "type": 1,
            "readDate":"2014-12-01T06:27:38.328Z"
        }, {
            "_id":"54764399ab43d1d4113abfd1",
            "text":"Am I dreaming?",
            "userId":"534b8e5aaa5e7afc1b23e69b",
            "date":"2014-11-26T21:18:17.591Z",
            "read":true,
            "type": 1,
            "readDate":"2014-12-01T06:27:38.337Z"
        }, {
            "_id":"547643aeab43d1d4113abfd2",
            "text":"Is this magic?",
            "userId":"534b8fb2aa5e7afc1b23e69c",
            "date":"2014-11-26T21:18:38.549Z",
            "read":true,
            "type": 1,
            "readDate":"2014-12-01T06:27:38.338Z"
        },{
            "_id":"547815dbab43d1d4113abfef",
            "text":"Gee wiz, this is something special.",
            "userId":"534b8e5aaa5e7afc1b23e69b",
            "date":"2014-11-28T06:27:40.001Z",
            "read":true,
            "type": 1,
            "readDate":"2014-12-01T06:27:38.338Z"
        }, {
            "_id":"54781c69ab43d1d4113abff0",
            "text":"I think I like Ionic more than I like ice cream!",
            "userId":"534b8fb2aa5e7afc1b23e69c",
            "date":"2014-11-28T06:55:37.350Z",
            "read":true,
            "type": 1,
            "readDate":"2014-12-01T06:27:38.338Z"
        }, {
            "_id":"54781ca4ab43d1d4113abff1",
            "text":"Yea, it's pretty sweet",
            "userId":"534b8e5aaa5e7afc1b23e69b",
            "date":"2014-11-28T06:56:36.472Z",
            "read":true,
            "type": 1,
            "readDate":"2014-12-01T06:27:38.338Z"
        }, {
            "_id":"5478df86ab43d1d4113abff4",
            "text":"Wow, this is really something huh?",
            "userId":"534b8fb2aa5e7afc1b23e69c",
            "date":"2014-11-28T20:48:06.572Z",
            "read":true,
            "type": 1,
            "readDate":"2014-12-01T06:27:38.339Z"
        }, {
            "_id":"54781ca4ab43d1d4113abff1",
            "text":"Create amazing apps - ionicframework.com",
            "userId":"534b8e5aaa5e7afc1b23e69b",
            "date":"2014-11-29T06:56:36.472Z",
            "read":true,
            "type": 1,
            "readDate":"2014-12-01T06:27:38.338Z"
        }, {
            "_id":"54781ca4ab43d1d4113abff1",
            "text":"Selection for Chair",
            "userId":"534b8fb2aa5e7afc1b23e69c",
            "date":"2014-11-29T06:56:36.472Z",
            "read":true,
            "type": 2,
            "readDate":"2014-12-01T06:27:38.338Z"
        }, {
            "_id":"54781ca4ab43d1d4113abff1",
            "text":"Dhiman_sutradhar.pdf",
            "userId":"534b8fb2aa5e7afc1b23e69c",
            "date":"2014-11-29T06:56:36.472Z",
            "read":true,
            "type": 3,
            "readDate":"2014-12-01T06:27:38.338Z"
        }, {
            "_id":"54781ca4ab43d1d4113abff1",
            "text":"Create amazing apps - ionicframework.com",
            "userId":"534b8e5aaa5e7afc1b23e69b",
            "date":"2014-11-29T06:56:36.472Z",
            "read":true,
            "type": 1,
            "readDate":"2014-12-01T06:27:38.338Z"
        }
    ],
    "unread":0
};