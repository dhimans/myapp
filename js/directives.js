angular.module('starter.directives', [])
.directive('tabRecent', function() {
    
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'templates/tab-recent.html',
        link: function(elem, attr, scope, ctrl) {
            
            
        }
    }
})
.directive('tabGroups', function() {
    
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'templates/tab-groups.html',
        link: function(elem, attr, scope, ctrl) {
            
            
        }
    }
})
.directive('tabContacts', function() {
    
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'templates/tab-contacts.html',
        link: function(elem, attr, scope, ctrl) {
            
            
        }
    }
})
.directive('tabFolders', function() {
    
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'templates/tab-folders.html',
        link: function(elem, attr, scope, ctrl) {
            
            
        }
    }
})

// directives
.directive('autolinker', ['$timeout',
  function($timeout) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        $timeout(function() {
          var eleHtml = element.html();

          if (eleHtml === '') {
            return false;
          }

          var text = Autolinker.link(eleHtml, {
            className: 'autolinker',
            newWindow: false
          });

          element.html(text);

          var autolinks = element[0].getElementsByClassName('autolinker');

          for (var i = 0; i < autolinks.length; i++) {
            angular.element(autolinks[i]).bind('click', function(e) {
              var href = e.target.href;
              console.log('autolinkClick, href: ' + href);

              if (href) {
                //window.open(href, '_system');
                window.open(href, '_blank');
              }

              e.preventDefault();
              return false;
            });
          }
        }, 0);
      }
    }
  }
]);