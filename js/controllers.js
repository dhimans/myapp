angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $ionicPopover) {

    // Form data for the login modal
    $scope.loginData = {};
    $scope.mainTab = 4;
    
    if(localStorage.getItem('userJSON') != '' && localStorage.getItem('userJSON') != null && localStorage.getItem('userJSON') != undefined) {
        $scope.user = JSON.parse(localStorage.getItem('userJSON'));
    }

    
    $ionicPopover.fromTemplateUrl('templates/popover.html', {
        scope: $scope,
        }).then(function(popover) {
        console.log("popover---------");
            $scope.popover = popover;
    });
      
    $scope.openPopover = function($event) {
        $scope.popover.show($event);
    };
    
    $scope.closePopover = function() {
        $scope.popover.hide();
    };
    
    //Cleanup the popover when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.popover.remove();
    });
    
    // Execute action on hidden popover
    $scope.$on('popover.hidden', function() {
        // Execute action
    });
    
    // Execute action on remove popover
    $scope.$on('popover.removed', function() {
        // Execute action
    });
    
    
    $scope.Logout = function() {
        $scope.user.isLoggedin = false;
        localStorage.setItem('userJSON', JSON.stringify($scope.user));
    }
    
    
    $scope.OpenProfile = function() {
        location.href = "#/app/profile";
    }
})


.controller('SplashCtrl', function($scope, $ionicModal, $timeout, $ionicPopover) {
    
    $scope.user = {};
    $scope.user.isLoggedin = false;
    
    if(localStorage.getItem('userJSON') != '' && localStorage.getItem('userJSON') != null && localStorage.getItem('userJSON') != undefined) {
        $scope.user = JSON.parse(localStorage.getItem('userJSON'));
    } else {
        
        $timeout(function() {
            location.href = "#/app/login";
        }, 3000);
    }

    $timeout(function() {
        if($scope.user.isLoggedin == 'true' || $scope.user.isLoggedin == true) {
            location.href = "#/app/lock";
        } else {
            location.href = "#/app/login";
        }
    }, 3000);
})


.controller('LoginCtrl', function($scope, $ionicModal, $timeout, $ionicPopover) {
    
    $scope.user = {};

    $scope.LoginUser = function() {
        $scope.user = {
            'firstName': 'Dhiman',
            'lastName': 'Sutradhar',
            'email': 'dhiman.sutradhar@forgeahead.io',
            'image': '',
            'isLoggedin': true,
            'isLockSet': true
        };
        
        localStorage.setItem('userJSON', JSON.stringify($scope.user));
        location.href = "#/app/set/lock";
        
    }
})

.controller('LockCtrl', function($scope, LoginService, $state, $stateParams) {
    
    var i = 1;
    // 1
    $scope.log_pattern = LoginService.getLoginPattern();
    
    // 2
    var lock = new PatternLock("#lockPattern", {
        // 3
        onDraw:function(pattern){
            // 4
            if ($scope.log_pattern) {
                // 5
                LoginService.checkLoginPattern(pattern).success(function(data) {
                    lock.reset();
                    $state.go('app.dashboard');
                }).error(function(data) {
                    lock.error();
                });
            } else {
                console.log("here--"+i);
                // 6
                LoginService.setLoginPattern(pattern);
                lock.reset();
                $scope.$apply(function() {
                    $scope.log_pattern = LoginService.getLoginPattern();
                });
            }
        }
    });
})

.controller('DashboardCtrl', function($scope, $state, $stateParams) {
    
    $scope.mainTab = 4;
    
    if(localStorage.getItem('employees') != '' && localStorage.getItem('employees') != null && localStorage.getItem('employees') != undefined) {
        $scope.employees = JSON.parse(localStorage.getItem('employees'));
    } else {
        $scope.employees = employeeJSON;
        localStorage.setItem('employees', JSON.stringify($scope.employees));
    }
    
    if(localStorage.getItem('groups') != '' && localStorage.getItem('groups') != null && localStorage.getItem('groups') != undefined) {
        $scope.groups = JSON.parse(localStorage.getItem('groups'));
    } else {
        $scope.groups = groupJSON;
        localStorage.setItem('groups', JSON.stringify($scope.groups));
    }
    
    
    $scope.toggleGroup = function(group) {
        group.show = !group.show;
    };
    $scope.isGroupShown = function(group) {
        return group.show;
    };
    
    $scope.OpenDetail = function(path) {
        location.href = path;
    }
    
    $scope.newEvent = function() {
        if ($scope.mainTab == 2) {
            location.href = "#/app/group/new";
        } else if ($scope.mainTab == 3) {
            location.href = "#/app/contact/new";
        } else {
            location.href = "#/app/apps";
        }
    }
    
    
    $scope.OpenChat = function(groupId, id, type) {
        localStorage.setItem('groupId', groupId);
        localStorage.setItem('id', id);
        localStorage.setItem('type', type);
        location.href = "#/app/chat";
    }
    
})


.controller('PollsCtrl', function($scope, LoginService, $state, $stateParams) {
    
    if(localStorage.getItem('polls') != '' && localStorage.getItem('polls') != null && localStorage.getItem('polls') != undefined) {
        $scope.polls = JSON.parse(localStorage.getItem('polls'));
    } else {
        $scope.polls = pollsJSON;
        localStorage.setItem('polls', JSON.stringify($scope.polls));
    }
    
    
    
    $scope.selectPoll = function(index) {
        location.href = "#/app/poll/" + index;
    }
    
    $scope.newPoll = function() {
        location.href = "#/app/new/poll";
    }

    
})


.controller('PollCtrl', function($scope, LoginService, $state, $stateParams) {
    
    if(localStorage.getItem('polls') != '' && localStorage.getItem('polls') != null && localStorage.getItem('polls') != undefined) {
        $scope.polls = JSON.parse(localStorage.getItem('polls'));
        $scope.poll = $scope.polls[$stateParams.id];
        console.log($scope.poll);
    }
    
    
    $scope.selectOption = function(option) {
        $scope.polls[$stateParams.id].votes = $scope.polls[$stateParams.id].votes + 1;
        $scope.polls[$stateParams.id].isAnswered = true;
        $scope.polls[$stateParams.id].answer = option.id;
        localStorage.setItem('polls', JSON.stringify($scope.polls));
    }
    
    
    
})


.controller('NewPollCtrl', function($scope, LoginService, $state, $stateParams) {
    
    if(localStorage.getItem('polls') != '' && localStorage.getItem('polls') != null && localStorage.getItem('polls') != undefined) {
        $scope.polls = JSON.parse(localStorage.getItem('polls'));
    }
    
    // 1
    $scope.count = 1;
    $scope.inputArr = [1, 2, 3, 4];
    $scope.poll = {
        'id': $scope.polls.length,
        'question': '',
        'completed': false,
        'votes': 0
    };
    $scope.options = [
        {
            'id': 'optn1',
            'optionText': '',
            'image': ''
        }, {
            'id': 'optn2',
            'optionText': '',
            'image': ''
        }, {
            'id': 'optn3',
            'optionText': '',
            'image': ''
        }, {
            'id': 'optn4',
            'optionText': '',
            'image': ''
        }
    ];
    
    
    
    $scope.getTimes=function(n){
        return new Array(n);
    };
    
    $scope.increseCnt = function() {
        $scope.inputArr = [];
        for(var i=0; i<$scope.count; i++) {
            $scope.inputArr.push(i);
        }
        //$scope.$apply();
    }
    
    $scope.AddPoll = function() {
        $scope.poll.options = $scope.options;
        $scope.polls.push($scope.poll);
        console.log($scope.poll);
        localStorage.setItem('polls', JSON.stringify($scope.polls));
        location.href = "#/app/polls";
    }
    
    
    
})


.controller('TodosCtrl', function($scope) {
    
    if(localStorage.getItem('todos') != '' && localStorage.getItem('todos') != null && localStorage.getItem('todos') != undefined) {
        $scope.todos = JSON.parse(localStorage.getItem('todos'));
    } else {
        $scope.todos = todosJSON;
    }
    
    
    $scope.completeTodo = function(index) {
        $scope.todos[index].completed = true;
        localStorage.setItem('todos', JSON.stringify($scope.todos));
    }
    
    $scope.openNewTodo = function() {
        location.href = "#/app/new/todo";
    }
      
})


.controller('NewTodoCtrl', function($scope) {
    
    if(localStorage.getItem('todos') != '' && localStorage.getItem('todos') != null && localStorage.getItem('todos') != undefined) {
        $scope.todos = JSON.parse(localStorage.getItem('todos'));
    }
    
    $scope.todo = {
        'id': 'todo' + $scope.todos.length + 1,
        'title': '',
        'completed': false
    };
    $scope.addTodo = function() {
        
        $scope.todos.push($scope.todo);
        localStorage.setItem('todos', JSON.stringify($scope.todos));
        location.href = "#/app/todos";
    }
      
})




.controller('NotesCtrl', function($scope, $state, $stateParams) {
    
    if(localStorage.getItem('notes') != '' && localStorage.getItem('notes') != null && localStorage.getItem('notes') != 'undefined') {
        $scope.notes = JSON.parse(localStorage.getItem('notes'));
    } else {
        $scope.notes = notesJSON;
        localStorage.setItem('notes', JSON.stringify($scope.notes));
    }
    
    
    
    $scope.selectNote = function(index) {
        location.href = "#/app/note/" + index;
    }
    
    $scope.newNote = function() {
        location.href = "#/app/new/note";
    }

    
})


.controller('NoteCtrl', function($scope, $state, $stateParams, textAngularManager) {
    
    $scope.version = textAngularManager.getVersion();
    $scope.versionNumber = $scope.version.substring(1);
    
    if(localStorage.getItem('notes') != '' && localStorage.getItem('notes') != null && localStorage.getItem('notes') != 'undefined') {
        $scope.notes = JSON.parse(localStorage.getItem('notes'));
        $scope.note = $scope.notes[$stateParams.id];
        console.log($scope.note);
    }
    
    
    $scope.selectOption = function(option) {
        
    }
    
    
    
})


.controller('NewNoteCtrl', function($scope, $state, $stateParams) {
    
    if(localStorage.getItem('notes') != '' && localStorage.getItem('notes') != null && localStorage.getItem('notes') != undefined) {
        $scope.notes = JSON.parse(localStorage.getItem('notes'));
    }
    $scope.note = {};
    
    
    $scope.AddNote = function() {
        $scope.note.id = $scope.notes.length;
        $scope.notes.push($scope.note);
        localStorage.setItem('notes', JSON.stringify($scope.notes));
        location.href = "#/app/notes";
    }
    
    
    
})


.controller('UserMessagesCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'MockService', 'CommandsService', '$ionicActionSheet', '$ionicPopup', '$ionicScrollDelegate', '$timeout', '$interval', '$ionicModal', function($scope, $rootScope, $state, $stateParams, MockService, CommandsService, $ionicActionSheet,
    $ionicPopup, $ionicScrollDelegate, $timeout, $interval, $ionicModal) {
    
    $scope.groupId = localStorage.getItem('groupId');
    $scope.id = localStorage.getItem('id');
    $scope.type = parseInt(localStorage.getItem('type'));
    
    if(localStorage.getItem('polls') != '' && localStorage.getItem('polls') != null && localStorage.getItem('polls') != undefined) {
        $scope.polls = JSON.parse(localStorage.getItem('polls'));
    } else {
        $scope.polls = pollsJSON;
        localStorage.setItem('polls', JSON.stringify($scope.polls));
    }
    
    if(localStorage.getItem('reminders') != '' && localStorage.getItem('reminders') != null && localStorage.getItem('reminders') != undefined) {
        $scope.reminders = JSON.parse(localStorage.getItem('reminders'));
    } else {
        $scope.reminders = remindersJSON;
        localStorage.setItem('reminders', JSON.stringify($scope.reminders));
    }
    
    if(localStorage.getItem('todos') != '' && localStorage.getItem('todos') != null && localStorage.getItem('todos') != undefined) {
        $scope.todos = JSON.parse(localStorage.getItem('todos'));
    } else {
        $scope.todos = todosJSON;
        localStorage.setItem('todos', JSON.stringify($scope.todos));
    }
    
    if($scope.type == 1) {
        if(localStorage.getItem('groups') != '' && localStorage.getItem('groups') != null && localStorage.getItem('groups') != undefined) {
            $scope.groups = JSON.parse(localStorage.getItem('groups'));
            $scope.group = search($scope.groupId, $scope.groups);
        }
        // mock acquiring data via $stateParams
        $scope.toUser = {
            _id: '534b8e5aaa5e7afc1b23e69b',
            pic: 'http://ionicframework.com/img/docs/venkman.jpg',
            username: $scope.group.teamName
        }
    } else {
        if(localStorage.getItem('employees') != '' && localStorage.getItem('employees') != null && localStorage.getItem('employees') != undefined) {
            $scope.employees = JSON.parse(localStorage.getItem('employees'));
            $scope.employee = search($scope.id, $scope.employees);
            console.log($scope.employee)
        }
        
        $scope.toUser = {
            _id: '534b8e5aaa5e7afc1b23e69b',
            pic: 'pics/' + $scope.employee.pic,
            username: $scope.employee.firstName + " " + $scope.employee.lastName
        }
    }

    // this could be on $rootScope rather than in $stateParams
    $scope.user = {
        _id: '534b8fb2aa5e7afc1b23e69c',
        pic: 'pics/dhiman.jpg',
        username: 'Dhiman'
    };

    $scope.input = {
        message: localStorage['userMessage-' + $scope.toUser._id] || ''
    };

    var messageCheckTimer;

    var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');
    var footerBar; // gets set in $ionicView.enter
    var scroller;
    var txtInput; // ^^^

    $scope.$on('$ionicView.enter', function() {
        console.log('UserMessages $ionicView.enter');

        getMessages();
      
        $timeout(function() {
            footerBar = document.body.querySelector('#userMessagesView .bar-footer');
            scroller = document.body.querySelector('#userMessagesView .scroll-content');
            txtInput = angular.element(footerBar.querySelector('textarea'));
        }, 0);

        messageCheckTimer = $interval(function() {
            // here you could check for new messages if your app doesn't use push notifications or user disabled them
        }, 20000);
    });

    $scope.$on('$ionicView.leave', function() {
        console.log('leaving UserMessages view, destroying interval');
        // Make sure that the interval is destroyed
        if (angular.isDefined(messageCheckTimer)) {
            $interval.cancel(messageCheckTimer);
            messageCheckTimer = undefined;
        }
    });

    $scope.$on('$ionicView.beforeLeave', function() {
        if (!$scope.input.message || $scope.input.message === '') {
            localStorage.removeItem('userMessage-' + $scope.toUser._id);
        }
    });

    function getMessages() {
        // the service is mock but you would probably pass the toUser's GUID here
        MockService.getUserMessages({
            toUserId: $scope.toUser._id
        }).then(function(data) {
            $scope.doneLoading = true;
            $scope.messages = data.messages;

            $timeout(function() {
                viewScroll.scrollBottom();
            }, 0);
        });
    }

    $scope.$watch('input.message', function(newValue, oldValue) {
        console.log('input.message $watch, newValue ' + newValue);
        if (!newValue) newValue = '';
        localStorage['userMessage-' + $scope.toUser._id] = newValue;
    });

    $scope.sendMessage = function(sendMessageForm) {
        var message = {};

        // if you do a web service call this will be needed as well as before the viewScroll calls
        // you can't see the effect of this in the browser it needs to be used on a real device
        // for some reason the one time blur event is not firing in the browser but does on devices
        keepKeyboardOpen();
        var text = $scope.input.message;
        
        if($scope.isCommandSelected == true) {
            
            if($scope.selectedCommand == 'reminder') {
                
                text = text.split(' ');
                var reminderText = text.splice(5, (text.length - 1)).join(" ");
                
                $scope.reminder = {
                    'id': 'reminder' + $scope.reminders.length + 1,
                    'message': reminderText,
                    'time': text[3] + " " + text[4],
                    'recipient': text[1],
                    'isCompleted': false
                };
                
                $scope.reminders.push($scope.reminder);
                localStorage.setItem('reminders', JSON.stringify($scope.reminders));
                message = {
                    toId: $scope.toUser._id,
                    text: reminderText,
                    type: 4
                };
                
            } else if($scope.selectedCommand == 'poll') {
                
                text = text.split('q:');
                var pollText = text[1];
                pollText = pollText.split('o:');
                var quesText = pollText[0];
                var options = pollText[1];
                options = options.split(', ');
                    
                $scope.poll = {
                    'id': 'poll' + $scope.polls.length + 1,
                    'question': quesText,
                    'completed': false,
                    'votes': 0,
                    'isAnswered': false,
                    'options': [
                        {
                            'id': 'optn1',
                            'optionText': options[0],
                            'image': ''
                        }, {
                            'id': 'optn2',
                            'optionText': options[1],
                            'image': ''
                        }, {
                            'id': 'optn3',
                            'optionText': options[2],
                            'image': ''
                        }, {
                            'id': 'optn4',
                            'optionText': options[3],
                            'image': ''
                        }
                    ]
                };
                
                $scope.polls.push($scope.poll);
                localStorage.setItem('polls', JSON.stringify($scope.polls));
                message = {
                    toId: $scope.toUser._id,
                    text: quesText,
                    type: 2
                };
                
            } else {
                
                text = text.split('t:');
                var todoText = text[1];
                $scope.todo = {
                    'id': 'todo' + $scope.todos.length + 1,
                    'title': todoText,
                    'completed': false
                };
                
                $scope.todos.push($scope.todo);
                localStorage.setItem('todos', JSON.stringify($scope.todos));
                message = {
                    toId: $scope.toUser._id,
                    text: todoText,
                    type: 5
                };
            }
            
            
        } else {
            
            message = {
                toId: $scope.toUser._id,
                text: $scope.input.message,
                type: 1
            };
            console.log("sdsfsdfsdfs")
            console.log(message)
        }
      
        //MockService.sendMessage(message).then(function(data) {
        $scope.input.message = '';

        message._id = new Date().getTime(); // :~)
        message.date = new Date();
        message.username = $scope.user.username;
        message.userId = $scope.user._id;
        message.pic = $scope.user.picture;
        
        console.log('message');
        console.log(JSON.stringify(message));
        
        $scope.messages.push(message);
        

        $timeout(function() {
            keepKeyboardOpen();
            viewScroll.scrollBottom(true);
        }, 0);

        $timeout(function() {
            $scope.messages.push(MockService.getMockMessage());
            keepKeyboardOpen();
            viewScroll.scrollBottom(true);
        }, 2000);

      //});
    };
    
    // this keeps the keyboard open on a device only after sending a message, it is non obtrusive
    function keepKeyboardOpen() {
        console.log('keepKeyboardOpen');
        txtInput.one('blur', function() {
            console.log('textarea blur, focus back on it');
            txtInput[0].focus();
        });
    }

    $scope.onMessageHold = function(e, itemIndex, message) {
        console.log('onMessageHold');
        console.log('message: ' + JSON.stringify(message, null, 2));
        $ionicActionSheet.show({
            buttons: [{
                text: 'Copy Text'
            }, {
                text: 'Delete Message'
            }],
            buttonClicked: function(index) {
                switch (index) {
                    case 0: // Copy Text
                        //cordova.plugins.clipboard.copy(message.text);

                        break;
                    case 1: // Delete
                        // no server side secrets here :~)
                        $scope.messages.splice(itemIndex, 1);
                        $timeout(function() {
                            viewScroll.resize();
                        }, 0);

                        break;
                }
          
                return true;
            }
        });
    };

    // this prob seems weird here but I have reasons for this in my app, secret!
    $scope.viewProfile = function(msg) {
        if (msg.userId === $scope.user._id) {
            // go to your profile
        } else {
            // go to other users profile
        }
    };
    
    // I emit this event from the monospaced.elastic directive, read line 480
    $scope.$on('taResize', function(e, ta) {
        console.log('taResize');
        if (!ta) return;
      
        var taHeight = ta[0].offsetHeight;
        console.log('taHeight: ' + taHeight);
      
        if (!footerBar) return;
      
        var newFooterHeight = taHeight + 10;
        newFooterHeight = (newFooterHeight > 44) ? newFooterHeight : 44;
      
        footerBar.style.height = newFooterHeight + 'px';
        scroller.style.bottom = newFooterHeight + 'px'; 
    });
    
      
    $scope.selectedChannel = search($scope.id, $scope.group.channels);
    console.log($scope.selectedChannel)
      
    $scope.toggleGroup = function(group) {
        group.show = !group.show;
          
    };
    $scope.isGroupShown = function(group) {
        return group.show;
    };
      
    $scope.selectList = function(channel, group) {
        $scope.selectedChannel = channel.title;
        $scope.toggleGroup(group);
    };
      
      
    $ionicModal.fromTemplateUrl('templates/modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) { 
        $scope.modal = modal;
    });
      
      
    $scope.openModal = function() {
          $scope.modal.show();
    }
      
      
    $scope.OpenDetail = function() {
          $(".main-menu").removeClass("block").addClass("none");
          $(".sub-menu").removeClass("none").addClass("block");
    }
      
      
    $scope.shareFile = function(fileName) {
        var message = {
            toId: $scope.toUser._id,
            text: fileName
        };
      
        //MockService.sendMessage(message).then(function(data) {
        $scope.input.message = '';

        message._id = new Date().getTime(); // :~)
        message.date = new Date();
        message.username = $scope.user.username;
        message.userId = $scope.user._id;
        message.pic = $scope.user.picture;

        $scope.messages.push(message);

        $timeout(function() {
            $scope.modal.hide();
            keepKeyboardOpen();
            viewScroll.scrollBottom(true);
        }, 0);

        $timeout(function() {
            $scope.messages.push(MockService.getMockMessage());
            keepKeyboardOpen();
            viewScroll.scrollBottom(true);
        }, 2000);

      //});
    };
    
    $scope.data = { "airlines" : [], "search" : '' };
    
    $scope.search = function() {
        
    	CommandsService.searchCommands($scope.input.message).then(
    		function(matches) {
    			$scope.data.commands = matches;
                if(matches.length > 0) {
                    $(".content-mask, .command-pane").removeClass("none").addClass("block");
                } else {
                    $(".content-mask, .command-pane").removeClass("block").addClass("none");
                }
    		}
    	)
    }
    
    
    $scope.selectCommand = function(command) {
        $(".content-mask, .command-pane").removeClass("block").addClass("none");
        $scope.isCommandSelected = true;
        $("#chat-box").val(command.title);
        $scope.selectedCommand = command.id;
    }
   

}])

// services
.factory('MockService', ['$http', '$q',
  function($http, $q) {
    var me = {};

    me.getUserMessages = function(d) {
      /*
      var endpoint =
        'http://www.mocky.io/v2/547cf341501c337f0c9a63fd?callback=JSON_CALLBACK';
      return $http.jsonp(endpoint).then(function(response) {
        return response.data;
      }, function(err) {
        console.log('get user messages error, err: ' + JSON.stringify(
          err, null, 2));
      });
      */
      var deferred = $q.defer();
      
		 setTimeout(function() {
      	deferred.resolve(getMockMessages());
	    }, 1500);
      
      return deferred.promise;
    };

    me.getMockMessage = function() {
      return {
        userId: '534b8e5aaa5e7afc1b23e69b',
        date: new Date(),
        type: 1,
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
      };
    }

    return me;
  }
])

// fitlers
.filter('nl2br', ['$filter',
  function($filter) {
    return function(data) {
      if (!data) return data;
      return data.replace(/\n\r?/g, '<br />');
    };
  }
]);

function onProfilePicError(ele) {
  this.ele.src = ''; // set a fallback
}

function getMockMessages() {
  return messageJSON;
}


function search(nameKey, myArray){
    for (var i=0; i < myArray.length; i++) {
        if (myArray[i].id == nameKey) {
            return myArray[i];
        }
    }
}