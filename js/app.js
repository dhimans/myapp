// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'starter.directives', 'ion-floating-menu', 'textAngular'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
  
  .state('app.splash', {
    url: '/splash',
    views: {
      'menuContent': {
        templateUrl: 'templates/splash.html',
        controller: 'SplashCtrl'
      }
    }
  })
  
  .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
      }
    }
  })
  
  .state('app.set', {
    url: '/set/lock',
    views: {
      'menuContent': {
        templateUrl: 'templates/set-lock.html',
        controller: 'LockCtrl'
      }
    }
  })
  
  .state('app.lock', {
    url: '/lock',
    views: {
      'menuContent': {
        templateUrl: 'templates/lock.html',
        controller: 'LockCtrl'
      }
    }
  })

  .state('app.dashboard', {
      url: '/dashboard',
      views: {
            'menuContent': {
                templateUrl: 'templates/dashboard.html',
                controller: 'DashboardCtrl'
            }
      }
  })

  .state('app.chat', {
      url: '/chat',
      views: {
            'menuContent': {
                templateUrl: 'templates/chat.html',
                controller: 'UserMessagesCtrl'
            }
      }
  })

  .state('app.polls', {
      url: '/polls',
      views: {
            'menuContent': {
                templateUrl: 'templates/poll-list.html',
                controller: 'PollsCtrl'
            }
      }
  })

  .state('app.poll', {
      url: '/poll/:id',
      views: {
            'menuContent': {
                templateUrl: 'templates/poll-detail.html',
                controller: 'PollCtrl'
            }
      }
  })

  .state('app.new', {
      url: '/new/poll',
      views: {
            'menuContent': {
                templateUrl: 'templates/new-poll.html',
                controller: 'NewPollCtrl'
            }
      }
  })

  .state('app.todos', {
      url: '/todos',
      views: {
            'menuContent': {
                templateUrl: 'templates/todo-list.html',
                controller: 'TodosCtrl'
            }
      }
  })

  .state('app.todo', {
      url: '/new/todo',
      views: {
            'menuContent': {
                templateUrl: 'templates/new-todo.html',
                controller: 'NewTodoCtrl'
            }
      }
  })

  .state('app.notes', {
      url: '/notes',
      views: {
            'menuContent': {
                templateUrl: 'templates/note-list.html',
                controller: 'NotesCtrl'
            }
      }
  })

  .state('app.note', {
      url: '/new/note',
      views: {
            'menuContent': {
                templateUrl: 'templates/new-note.html',
                controller: 'NewNoteCtrl'
            }
      }
  })

  .state('app.detail', {
      url: '/note/:id',
      views: {
            'menuContent': {
                templateUrl: 'templates/note-detail.html',
                controller: 'NoteCtrl'
            }
      }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/notes');
});
